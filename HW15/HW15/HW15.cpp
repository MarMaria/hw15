﻿// HW15.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>

void PrintNumbers(int max, int start_num)
{
	for (int i = start_num; i <= max; i += 2)
	{
		std::cout << i << '\t';
	}
	std::cout << '\n';
}

int main()
{
	int start_even = 0; //чётное
	int start_odd = 1; //нечётное

	// part1
	const int max_main = 10;
	std::cout << "Max number = " << max_main << '\n';
	for (int i = start_even; i <= max_main; i += 2)
	{
		std::cout << i << '\t';
	}
	std::cout << "\n\n\n";

	// part2
	int max_custom;
	char type;

	std::cout << "Enter max number\n";
	std::cin >> max_custom;
	std::cout << "Show even(press 'e') or odd(press 'o') numbers\n";
	std::cin >> type;

	if (type == 'e')
	{
		PrintNumbers(max_custom, start_even);
	}
	else if (type == 'o')
	{
		PrintNumbers(max_custom, start_odd);
	}
	else
	{
		std::cout << "Wrong command. Do nothing\n";
	}

}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
